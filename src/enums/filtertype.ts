export enum FilterType {
    Price = 'price',
    Model = 'model',
    FuelType = 'fuel_type',
    Transmission = 'transmission',
    MinYear = 'min_year',
    BodyType = 'body_type',
    MaxMileage = 'max_mileage',
    Color = 'color'
}

export interface ModelPriceFilterData
{
    min_price: number;
    max_price: number;
    count: number;
    display_name: string;
}

export interface ModelFilterData
{
    value: string;
    display_name: string;
    count: string;
}

export interface Filter
{
    type: string;
    data: ModelPriceFilterData[] | ModelFilterData[]
}

