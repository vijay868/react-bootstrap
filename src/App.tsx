import 'bootstrap/scss/bootstrap.scss';
import 'font-awesome/scss/font-awesome.scss';
import Header from './components/layout/header';
import Home from './components/home';
import Leftmenu from './components/layout/leftmenu';

import './app.scss';

function App() {
  return (
    <>
      <Header></Header>
      <div className="container-fluid appContainer">
        <div className="row">
          <div className="appLeftMenuContainer">
            <Leftmenu></Leftmenu>
          </div>
          <div className="appHomeContainer">
            <Home></Home>
          </div>
        </div>
      </div>
    </>
  );
}

export default App;
