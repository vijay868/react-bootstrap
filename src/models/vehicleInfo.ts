

export interface VehicleInfo {
    id: number;
    make: string;
    model: string;
    variant: string;
    images: VehicleImagesInfo[];
    mileage: number;
    make_year: number;
    price: number;
    added_on: string;
    permanent_url: string;
    sold: boolean;
    locality: string;
    city: string;
    fuel_type: string;
    no_of_owners: number;
    color: string;
    score: number;
    rto: string;
    rto_state: string;
    seller_type: string;
    new_car: NewCarInfo;
    main_image: ImageInfo;
    thumbnail: ImageInfo;
    transmission: string;
    hub: string;
    on_road_price: number;
    booked: boolean;
    upcoming: boolean;
    expected_live_date: Date;
    is_fixed_price: boolean;
    home_test_drive_available: boolean;
    showroom_visit_available: boolean;
    has_three_sixty_view: boolean;
    applicable_insurance_validity: {};
    on_hold: boolean;
    hold_end_time: Date;
}

export interface VehicleImagesInfo {
    file_label: string;
    file: ImagesInfo;
}

export interface ImagesInfo {
    moburl: string;
    thumburl: string;
    taburl: string;
    absurl: string;
}

export interface NewCarInfo {
    on_road_price: number;
    active: boolean;
    description: string;
    blogs: NewCarBlogs[];
    depreciation: number;
}

export interface NewCarBlogs {
    title: string;
    description: string;
    url: string;
}

export interface ImageInfo {
    url: string;
}

export interface VehicleInsuranceInfo {
    month: number;
    year: number;
    free: boolean;
}