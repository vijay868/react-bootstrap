import React from 'react';
import { VehicleInfo } from '../models/vehicleInfo';

export class VehicleCard extends React.Component<{ vehicleInfo: VehicleInfo }> {
    private getTrimmedName(value: string): string {
        if (value.length >= 35)
            return value.substr(0, 35) + '...';
        return value;
    }
    public render() {
        return (
            <div className="col-3">
                <div className="vehicleInfoMask">
                    <img className="img-fluid img-thumbnail" src={this.props.vehicleInfo.main_image.url} />
                    <div className="vehicleInfoCss">
                        <div className="title">{this.getTrimmedName(`${this.props.vehicleInfo.make} ${this.props.vehicleInfo.model} ${this.props.vehicleInfo.variant}`)}</div>
                        <div className="vehicleConfigurationCss">
                            {this.props.vehicleInfo.mileage} Kms. {this.props.vehicleInfo.fuel_type === 'petrol' ? 'Petrol' : 'Desel'}. {this.props.vehicleInfo.transmission === 'manuel' ? 'Manuel' : 'Automatic'} <br />
                        </div>
                        <div className="vehiclePriceCss">Rs. {this.props.vehicleInfo.price} /-</div>
                        <div className="vehicleLocationCss">
                            <div><i className="fa fa-map-marker" />&nbsp;&nbsp;{this.props.vehicleInfo.hub}</div>
                            <div><i className="fa fa-home" />&nbsp;{this.props.vehicleInfo.home_test_drive_available ? 'Home Test Drive Available' : ''}</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default VehicleCard;