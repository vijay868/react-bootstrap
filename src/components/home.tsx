import React, { useState } from 'react';
import { VehicleInfo } from '../models/vehicleInfo';
import { getData } from '../services/data';
import VehicleCard from './vehiclecard';
import './home.scss';

export interface HomeViewModel {
    vehicles: VehicleInfo[];
}

class Home extends React.Component<{}, HomeViewModel> {
    public constructor(props: {}) {
        super(props);
        this.state = {
            vehicles: []
        };
    }

    public async componentDidMount() {
        const data = await getData('hyderabad', 'cars', 'used', false, 1);
        const vehiclesInfoArray = data.results as VehicleInfo[];
        this.setState({
            vehicles: vehiclesInfoArray
        });
    }

    public render() {
        return (
            <div className="row homeContainer">
                {this.state.vehicles.map((vehicleInfo: VehicleInfo) => (
                    <VehicleCard vehicleInfo={vehicleInfo}></VehicleCard>
                ))}
            </div>
        );
    }
}

export default Home;