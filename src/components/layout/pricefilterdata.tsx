import React from 'react';
import { Filter, ModelPriceFilterData } from '../../enums/filtertype';
import './filterdata.scss'


class PriceFilterData extends React.Component<{ filter: Filter, index: number }, { data: ModelPriceFilterData[], toggle: boolean }> {
    constructor(props: { filter: Filter, index: number }) {
        super(props);
        this.state = {
            data: this.props.filter.data as ModelPriceFilterData[],
            toggle: true
        }
    }

    toggle() {
        this.setState({
            toggle: !this.state.toggle
        });
    }

    public render() {
        return (
            <div className="filterDataContainer">
                <div className="title" onClick={() => this.toggle()}>
                    Price
                    <span className="toggleIcon">
                        <i className={`${this.state.toggle ? 'fa fa-arrow-down' : 'fa fa-arrow-up'}`}></i>
                    </span>
                </div>
                <div className={this.state.toggle ? 'show' : 'hide'}>
                    {this.state.data.map((item: ModelPriceFilterData, index: number) => (
                        <div key={index}>
                            <input type="checkbox" id={item.display_name} />&nbsp;<label htmlFor={item.display_name}>{item.display_name}</label>
                        </div>
                    ))}
                </div>
            </div>
        )
    }
}

export default PriceFilterData;