import React from 'react';
import { Filter, ModelFilterData } from '../../enums/filtertype';
import './filterdata.scss';

class GenericFilterData extends React.Component<{ filter: Filter, index: number }, { data: ModelFilterData[], toggle: { [key: number]: boolean } }> {
    constructor(props: { filter: Filter, index: number }) {
        super(props);
        this.state = {
            data: this.props.filter.data as ModelFilterData[],
            toggle: {}
        }
    }

    public toggle(index: number) {
        const value = this.getByIndex(index);
        if (value === undefined) {
            this.add(index, true);
        } else {
            this.state.toggle[index] = !this.getByIndex(index);
        }
        this.setState({
            toggle: this.state.toggle
        });
    }

    private add(index: number, value: boolean) {
        this.state.toggle[index] = value;
    }

    private getByIndex(index: number) {
        return this.state.toggle[index];
    }

    public render() {
        return (
            <div className="filterDataContainer">
                <div className="title" onClick={() => this.toggle(this.props.index)}>
                    {
                        {
                            'model': 'Models',
                            'fuel_type': 'Fuel Type',
                            'transmission': 'Transmission',
                            'min_year': 'Min Year & above',
                            'body_type': 'Body Type',
                            'max_mileage': 'Max Mileage',
                            'color': 'Color'
                        }[this.props.filter.type] || ''
                    }
                    <span className="toggleIcon">
                        <i className={`${this.state.toggle[this.props.index] ? 'fa fa-arrow-down' : 'fa fa-arrow-up'}`}></i>
                    </span>
                </div>
                <div className={this.state.toggle[this.props.index] ? 'show' : 'hide'}>
                    {this.state.data.map((item: ModelFilterData, index: number) => (
                        (   this.props.filter.type === 'fuel_type' ||
                            this.props.filter.type === 'min_year' ||
                            this.props.filter.type == 'max_mileage') ?
                            <div key={index}>
                                <input type="radio" id={`${item.display_name}_${index}`} name={this.props.filter.type} />&nbsp;<label htmlFor={`${item.display_name}_${index}`}>{item.display_name}</label>
                            </div> :
                            <div key={index}>
                                <input type="checkbox" id={`${item.display_name}_${index}`} />&nbsp;<label htmlFor={`${item.display_name}_${index}`}>{item.display_name}</label>
                            </div>
                    ))}
                </div>
            </div>
        );
    }
}

export default GenericFilterData;