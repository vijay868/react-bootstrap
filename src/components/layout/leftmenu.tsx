import React from 'react';
import { Filter } from '../../enums/filtertype';
import { getFilters } from '../../services/filter';
import GenericFilterData from './genericfilterdata';
import PriceFilterData from './pricefilterdata';
import './leftmenu.scss';


export interface IState {
    filters: Filter[]
}

class Leftmenu extends React.Component<{}, IState> {
    public constructor(props: {}) {
        super(props);
        this.state = {
            filters: []
        }
    }

    public async componentDidMount() {
        this.setState({ filters: await getFilters('cars', 'used') });
    }

    public render() {
        return (
            <div className="leftMenuContainer">
                {
                    this.state.filters.map((filterType, index) => (
                        {
                            'price': <div key={index}><PriceFilterData filter={filterType} index={index} /></div>
                        }[filterType.type] || <div key={index}><GenericFilterData filter={filterType} index={index} /></div>
                    ))
                }
            </div>
        )
    }
}

export default Leftmenu;