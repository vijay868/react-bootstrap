import { VehicleInfo } from '../models/vehicleInfo';


export const getData = async (city: string, productType: string, category: string, isIncludedBooking: boolean, page: number) => {
    const data = await (await fetch(`https://api.spinny.com/api/c/listings/?city=${city}&product_type=${productType}&category=${category}&include_booked=${isIncludedBooking}&page=${page}`)).json();
    return data;
}

/*
class VehicleInfoService {
    async getData(city: string, productType: string, category: string, isIncludedBooking: boolean, page: number) {
        
        const response: Promise<Response> = await fetch(`https://api.spinny.com/api/c/listings/?city=${city}&product_type=${productType}&category=${category}&include_booked=${isIncludedBooking}&page=${page}`);
        response.then((value: any) => {

        })
        

        const data = await (await fetch(`https://api.spinny.com/api/c/listings/?city=${city}&product_type=${productType}&category=${category}&include_booked=${isIncludedBooking}&page=${page}`)).json();
        return data as VehicleInfo;
    }
}
*/