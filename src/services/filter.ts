


export const getFilters = async (productType: string, category: string) => {
    const data = await (await fetch(`https://api.spinny.com/api/c/listings/filters/multi_smart/?city=hyderabad&product_type=${productType}&category=${category}`)).json();
    return data;
}